export default {
    common: {
        list: 'Function List',
        keyword: 'Keyword',
        operator: 'Operator',
        function: 'Function',
        example: 'Example',
        import: 'Import Data',
        importFromUrl: 'Import from URL',
        create: 'Create Function',
        modify: 'Modify Function [ $NAME ]',
    },
    tip: {
        selectPluginHolder: 'Please select a plugin',
        selectTypeHolder: 'Please select a type'
    }
}