export default {
    common: {
        list: 'Menu List',
        create: 'Create Menu',
        modify: 'Modify Menu [ $NAME ]',
        parent: 'Parent Menu',
        redirect: 'Redirect Menu',
        new: 'New Menu',
        i18nKey: 'I18N Key',
        icon: 'Icon',
    },
    tip: {
        selectType: 'Please select a type',
        selectParent: 'Please select a parent',
        selectRedirect: 'Please select a redirect'
    }
}