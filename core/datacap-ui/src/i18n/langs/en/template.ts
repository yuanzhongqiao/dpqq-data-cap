export default {
    common: {
        list: 'Template List',
        create: 'Create Template',
        modify: 'Modify Template [ $NAME ]',
    }
}