export default {
    common: {
        list: '函数列表',
        keyword: '关键字',
        operator: '运算符',
        function: '函数',
        example: '示例',
        import: '导入数据',
        importFromUrl: '从 URL 导入',
        create: '创建函数',
        modify: '修改函数 [ $NAME ]',
    },
    tip: {
        selectPluginHolder: '请选择插件',
        selectTypeHolder: '请选择类型'
    }
}