export default {
    common: {
        list: '菜单列表',
        create: '创建菜单',
        modify: '修改菜单 [ $NAME ]',
        parent: '父菜单',
        redirect: '重定向菜单',
        new: '新菜单',
        i18nKey: '国际化标识',
        icon: '图标'
    },
    tip: {
        selectType: '请选择类型',
        selectParent: '请选择父菜单',
        selectRedirect: '请选择重定向菜单'
    }
}